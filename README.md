# QA Challenge

Desafio Easy Carros para analista de QA.


## Introdução

Você está participando do processo para integrar o time de Produto e Tecnologia da [Easy Carros](https://easycarros.com/).

Este desafio tem como objetivo avaliar suas *skills* na criação de testes para uma solução do mundo real.


## O que é a Easy Carros?

A Easy Carros surgiu como uma plataforma de marketplace para serviços automotivos.

Hoje somos um hub que oferece diversos serviços para o mercado de mobilidade de logística.


## O desafio

Você deve ter recebido por email um usuário e senha para acesso à nossa plataforma.

Seu desafio é criar alguns _test cases_ para as funcionalidades reais presentes no nosso sistema.

Para isso, será necessário navegar pela aplicação e entender as funcionalidades existentes. Então descrever um _use case_
**hipotético** conforme o seu entendimento da funcionalidade e criar um _test case_ para o mesmo.


Abaixo segue um exemplo de _use case_ para o qual deve desenvolver um _test case_.


### 1. Usuário insere veículo na plataforma

> Eu, como cliente da Easy Carros, gostaria de inserir veículos na plataforma para gerenciar minha frota. Para isso 
> realizo os seguintes passos:
>
>   1. Acesso a tela de Veículos;
>   2. Clico em "Adicionar Veículos";
>   3. Digito a placa e renavam do veículo separados por espaço e cada veículo separado por linha;
>   4. Clico em "Carregar veículos" e aguardo o processamento concluir.


## As regras do jogo

- Escreva ao menos 3 _test cases_.
- Use a linguagem, tecnologia, plataforma, ferramenta ou qualquer outra opção com a qual você se sente mais confortável.
- Assuma que todos os dados são válidos (não há necessidade de testes de validação).
- Envie sua solução para um repositório público para leitura (Github, Bitbucket, Gitlab, etc.) no formato que preferir.
- Crie um arquivo `README` ou outro HOW-TO com instruções detalhadas de como executar os _test cases_.

### Bônus

- Ao menos 1 _use case/test case_ para cada um dos três principais contextos do sistema (Veículos, Condutores e Infrações).


### Como vou ser avaliado?

Vamos analisar sua solução com respeito a:

- Aplicabilidade dos _test cases_ (se funcionam de fato com a aplicação testada)
- Otimização dos testes (eficácia e eficiência)
- Separação de contextos/ciclos de vida (completo ou parcial)


O que **NÃO** vamos analisar:

- Use cases 
    - Não precisam estar aderentes com os requisitos reais
    - Só servirão para o seu entendimento da solução
- Performance
- Escolha da tecnologia A em vez da B


## Para onde enviar seu repositório

Envie um email para `tech@easycarros.com` com o assunto `Desafio QA Challenge - [SEU NOME]` contendo o link para o repositório que você criou.
